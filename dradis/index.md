---
Title: dradis
Homepage: https://dradisframework.org
Repository: https://gitlab.com/kalilinux/packages/dradis
Architectures: amd64
Version: 4.9.0-0kali1
Metapackages: kali-linux-everything kali-linux-large kali-tools-reporting 
Icon: images/dradis-logo.svg
PackagesInfo: |
 ### dradis
 
  Dradis is a tool to help in the process of penetration testing. Penetration
  testing is about information:
   
   1. Information discovery
   2. Exploit useful information
   3. Report the findings
   
  But penetration testing is also about sharing the information you and your
  teammates gather. Not sharing the information available in an effective way
  will result in exploitation oportunities lost and the overlapping of efforts.
 
 **Installed size:** `604.77 MB`  
 **How to install:** `sudo apt install dradis`  
 
 {{< spoiler "Dependencies:" >}}
 * adduser
 * bundler
 * git
 * libc6 
 * libgcc-s1 
 * libpq5 
 * libruby3.1 
 * libstdc++6 
 * lsof
 * pwgen
 * ruby 
 * zlib1g 
 {{< /spoiler >}}
 
 ##### dradis
 
 
 ```
 root@kali:~# dradis -h
 [i] Something is already using port: 3000/tcp
 COMMAND    PID   USER   FD   TYPE DEVICE SIZE/OFF NODE NAME
 ruby3.1 113790 dradis   13u  IPv6 252190      0t0  TCP localhost:3000 (LISTEN)
 ruby3.1 113790 dradis   14u  IPv4 252191      0t0  TCP localhost:3000 (LISTEN)
 
 UID          PID    PPID  C STIME TTY      STAT   TIME CMD
 dradis    113790       1 33 10:57 ?        Ssl    0:01 /usr/bin/ruby3.1 bin/rails server
 
 [*] Please wait for the Dradis service to start.
 [*]
 [*] You might need to refresh your browser once it opens.
 [*]
 [*]  Web UI: http://127.0.0.1:3000
 
 ```
 
 - - -
 
 ##### dradis-stop
 
 
 ```
 root@kali:~# dradis-stop -h
 * dradis.service - Dradis web application
      Loaded: loaded (/lib/systemd/system/dradis.service; disabled; preset: disabled)
      Active: inactive (dead)
 
 Aug 11 10:57:36 kali bundle[113790]:         /usr/lib/dradis/ruby/3.1.0/gems/bootsnap-1.16.0/lib/bootsnap/load_path_cache/core_ext/kernel_require.rb:32:in `require'
 Aug 11 10:57:36 kali bundle[113790]:         /usr/lib/dradis/ruby/3.1.0/gems/bootsnap-1.16.0/lib/bootsnap/load_path_cache/core_ext/kernel_require.rb:32:in `require'
 Aug 11 10:57:36 kali bundle[113790]:         bin/rails:5:in `<main>'
 Aug 11 10:57:37 kali bundle[113790]: [2023-08-11 10:57:36] INFO  going to shutdown ...
 Aug 11 10:57:37 kali bundle[113790]: [2023-08-11 10:57:36] INFO  WEBrick::HTTPServer#start done.
 Aug 11 10:57:37 kali bundle[113790]: Exiting
 Aug 11 10:57:36 kali systemd[1]: Stopping dradis.service - Dradis web application...
 Aug 11 10:57:37 kali systemd[1]: dradis.service: Deactivated successfully.
 Aug 11 10:57:37 kali systemd[1]: Stopped dradis.service - Dradis web application.
 Aug 11 10:57:37 kali systemd[1]: dradis.service: Consumed 1.732s CPU time.
 ```
 
 - - -
 
---
{{% hidden-comment "<!--Do not edit anything above this line-->" %}}

## Screenshots

```
service dradis start
```

![dradis](images/dradis.png)
