---
Title: imhex
Homepage: https://github.com/WerWolv/ImHex
Repository: https://gitlab.com/kalilinux/packages/imhex
Architectures: amd64 arm64
Version: 1.30.1+ds-0kali1
Metapackages: kali-linux-everything 
Icon: /images/kali-tools-icon-missing.svg
PackagesInfo: |
 ### imhex
 
  This package contains a Hex Editor for Reverse Engineers, Programmers and
  people who value their retinas when working at 3 AM.
 
 **Installed size:** `25.94 MB`  
 **How to install:** `sudo apt install imhex`  
 
 {{< spoiler "Dependencies:" >}}
 * libc6 
 * libcapstone4 
 * libcurl4 
 * libdbus-1-3 
 * libfmt9 
 * libfreetype6 
 * libgcc-s1 
 * libglfw3 
 * libmagic1 
 * libmbedcrypto7 
 * libmbedtls14 
 * libmbedx509-1 
 * libopengl0
 * libstdc++6 
 * libyara10 
 {{< /spoiler >}}
 
 ##### imhex
 
 
 
 - - -
 
---
{{% hidden-comment "<!--Do not edit anything above this line-->" %}}
