---
Title: jsql
Homepage: https://github.com/ron190/jsql-injection
Repository: https://gitlab.com/kalilinux/packages/jsql
Architectures: all
Version: 0.90-0kali1
Metapackages: kali-linux-everything kali-linux-large kali-tools-database kali-tools-web 
Icon: images/jsql-logo.svg
PackagesInfo: |
 ### jsql-injection
 
  jSQL Injection is a lightweight application used to find database information
  from a distant server.
  jSQL is free, open source and cross-platform (Windows, Linux, Mac OS X,
  Solaris).
 
 **Installed size:** `8.22 MB`  
 **How to install:** `sudo apt install jsql-injection`  
 
 {{< spoiler "Dependencies:" >}}
 * default-jre
 * java-wrappers
 {{< /spoiler >}}
 
 ##### jsql
 
 
 ```
 root@kali:~# jsql -h
 2023-08-14 07:06:35,667 ERROR [main] jsql.MainApplication (MainApplication.java:30) - Headless runtime detected, please install or use default Java runtime instead of headless runtime
 ```
 
 - - -
 
 ##### jsql-injection
 
 
 ```
 root@kali:~# jsql-injection -h
 2023-08-14 07:06:37,202 ERROR [main] jsql.MainApplication (MainApplication.java:30) - Headless runtime detected, please install or use default Java runtime instead of headless runtime
 ```
 
 - - -
 
---
{{% hidden-comment "<!--Do not edit anything above this line-->" %}}

## Screenshots

```
jsql
```

![jsql](images/jsql.png)
