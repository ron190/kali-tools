---
Title: kali-autopilot
Homepage: https://gitlab.com/kalilinux/packages/kali-autopilot
Repository: https://gitlab.com/kalilinux/packages/kali-autopilot
Architectures: all
Version: 3.1
Metapackages: 
Icon: images/kali-autopilot.svg
PackagesInfo: |
 ### kali-autopilot
 
  Kali Autopilot is a tool to help develop automatic attack scripts
  for red and purple teaming.
   
  It is primarily intended to create scripts that attack vulnerable
  machines in the Kali Purple platform for detection and response
  training but it is also useful for creating scripts used for
  penetration testing.
 
 **Installed size:** `95 KB`  
 **How to install:** `sudo apt install kali-autopilot`  
 
 {{< spoiler "Dependencies:" >}}
 * python3
 * python3-easygui
 * python3-pymetasploit3
 * python3-sarge
 * python3-wxgtk4.0
 {{< /spoiler >}}
 
 ##### kali-autopilot
 
 
 
 - - -
 
---
{{% hidden-comment "<!--Do not edit anything above this line-->" %}}
